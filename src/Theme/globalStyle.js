import { createGlobalStyle } from 'styled-components';
// This is the dynamic styled-component for ALL divs within App.js, any css
// property that needs to be overridden on a div should go here.
// Any other properties that need to be overriden for a specific component should
// be defined in that components corresponding styled<InsertComponentHere>.js file.
// props.theme is a global object that can be accessed by any child of App.js

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    outline: none;
    }
  body, html {
    font-family: ${props =>
        props.theme.fonts.primary__font
    }};
    color: ${props => props.theme.colors.text__color};
    padding: 0;
    margin: 0;
    height: 100%;
    background: ${props => props.theme.colors.background__color};
    overflow: auto;
  }
  h1 {
    ${props => props.theme.text.heading1}
  }
  h2 {
    ${props => props.theme.text.heading2}
  }
  h3 {
    ${props => props.theme.text.heading3}
  }
  h4 {
    ${props => props.theme.text.heading4}
  }
  h5 {
    ${props => props.theme.text.heading5}
  }
  h6 {
    ${props => props.theme.text.heading6}
  }

  .tooltipContent {
    background-color: #70828e !important;
    z-index: 2100000005 !important
    font-family: Heebo;
    font-size: 14px;
    max-width: 200px;
  }
  .__react_component_tooltip.type-info {
    border: none !important;
    opacity: 1;
    ::after {
      border: none !important;
      opacity: 1;
    }
  }
`;

export default GlobalStyle;
