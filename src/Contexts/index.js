import React from 'react';
import { CUIProvider } from './cuiContext';

const GlobalProvider = ({ children }) => {
    return <CUIProvider>{children}</CUIProvider>;
};

export default GlobalProvider;
