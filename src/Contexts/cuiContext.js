import React, { createContext, useContext, useReducer } from 'react';

const initialState = {
    userCUIs: [],
};

const reducer = ({ userCUIs }, action) => {
    switch (action.type) {
        case 'INIT':
            const newCUIs = { userCUIs: action.payload.userCuis };
            return newCUIs;
        case 'UPDATE':
            return { userCUIs: action.payload.userCuis };
        default:
            return { userCUIs };
    }
};

export const CUIContext = createContext();

export const CUIProvider = ({ children }) => (
    <CUIContext.Provider value={useReducer(reducer, initialState)}>{children}</CUIContext.Provider>
);

export const useCUI = () => useContext(CUIContext);
