import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, withRouter } from 'react-router-dom';
import 'unfetch/polyfill';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App.jsx';
import * as serviceWorker from './serviceWorker';


const AppWithRouter = withRouter(App);

// Wraps the entire APP in GA to track routes
const AppWithGA = () => (
    <BrowserRouter>
        <AppWithRouter />
    </BrowserRouter>
);


ReactDOM.render(<AppWithGA />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
