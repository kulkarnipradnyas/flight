import React from 'react';
import { Redirect } from 'react-router-dom';

// This component is used to wrap any components that should be protected via authentication
export default ({ component: Component, auth, source, ...rest }) => {
    return (
        <div className="container">
            <Component auth={auth} source={source} {...rest} />
        </div>
    );
    // Redirect to callback for it to check the server for a session.
    // return <Redirect to={{ pathname: '/callback', state: { referrer: rest.location || '' } }} />;
};
