import React, { useState, useEffect } from 'react';
import { ThemeProvider } from 'styled-components';
import { Router, Route, Switch } from 'react-router-dom';


import GlobalProvider from './Contexts/index';
import { ModalProvider } from 'styled-react-modal';
import { FadingBackground } from './Component/Modal/modal'
import './App.css';
import themes from './Theme/theme';
import Spinner from './Component/Spinner';
import GlobalStyle from './Theme/globalStyle'
import SecuredRoute from './Container/SecuredRoute';
import Flight from './Container/Flight'
import history from './Services/history'

const App = () => {

  const [fetching, setFetching] = useState(false);
  const [theme, setTheme] = useState(themes);


  useEffect(() => {
    setTheme(themes)
  })
  return fetching ?
    <ThemeProvider theme={theme}>
      <Spinner fullscreen />
    </ThemeProvider>
    :
    <Router history={history}>
      <GlobalProvider>
        <ThemeProvider theme={theme}>
          <ModalProvider backgroundComponent={FadingBackground}>
            <GlobalStyle />
            <Switch>
              <Route
                path="/login"
                exact
                render={() => {
                  return <div >login</div>;
                }}
              />
              <Route
                path="/"
                exact
                render={() => {
                  return <SecuredRoute
                    component={Flight} />
                }}

              />
            </Switch>
          </ModalProvider>
        </ThemeProvider>
      </GlobalProvider>
    </Router >
}



export default App;
