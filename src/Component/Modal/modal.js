import styled, { css } from 'styled-components/macro';
import Modal, { BaseModalBackground } from 'styled-react-modal';

export const StyledModal = Modal.styled`
  display: flex;
  align-items: center;
  overflow-y: auto;
  justify-content: center;
  background-color: white;
  transition: opacity ease 500ms;
  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 38px 3px rgba(0, 0, 0, 0.14),
  0px 9px 46px 8px rgba(0, 0, 0, 0.12);
  margin: auto;
  border-radius: 4px;
  position: relative;
  max-height: 90vh;
  max-width: ${({ modalWidth }) => modalWidth};
  width: ${({ modalWidth }) => (modalWidth ? '100%' : 'fit-content')};
  margin-left: 12px;
  margin-right: 12px;

  ${({ fullPage }) =>
        fullPage &&
        css`
      max-height: 100vh;
      margin: 0px;
      margin-left: 0px;
      margin-right: 0px;
      border-radius: 0px;
    `}
`;

export const ModalContent = styled.div`
  width: ${({ modalWidth }) => modalWidth};
  max-height: 90vh;

  ${({ fullPage }) =>
        fullPage &&
        css`
      height: 100%;
      max-height: 100%;
    `}
`;

export const FadingBackground = styled(BaseModalBackground)`
  opacity: ${props => props.opacity};
  transition: opacity ease 200ms;
  z-index: 2100000003;
`;
