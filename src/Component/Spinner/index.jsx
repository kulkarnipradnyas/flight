import React from 'react';
import { StyledSpinner, SpinnerContainer } from './styledSpinner';
import { Spinner } from 'react-bootstrap'

const Spinners = props => (
    <SpinnerContainer {...props}>
        <StyledSpinner
            animation="border"
            style={{
                position: props.fullscreen && 'absolute',
                margin: 'auto',
                top: '0',
                left: '0',
                bottom: '0',
                right: '0',
            }}
            spinnercolor={'#2EA5F3'}
        />
    </SpinnerContainer>
);

export default Spinners;
