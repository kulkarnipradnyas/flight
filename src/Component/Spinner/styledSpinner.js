import styled from 'styled-components';
import CircularProgress from 'react-bootstrap/Spinner';

export const SpinnerContainer = styled.div`
  ${props => {
    if (props.fullscreen) {
      return `
        width: 100%;
        height: 100vh;
      `;
    }
    if (props.subpage) {
      return `
        height: 64vh;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
      `;
    }
    return `
      width: 100%;
      height: 100%;
    `;
  }};
`;

export const StyledSpinner = styled(CircularProgress)`
  && {
    color: ${props => props.spinnercolor};
    width: 100px;
    height: 100px;
    > * {
      ${props => props.theme.spinner};
    }
  }
`;
